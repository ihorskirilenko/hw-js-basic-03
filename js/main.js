'use strict'

/*
Теорія

1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
Для повтору за умовою або фіксованої кількості разів виконання дій над даними
(наприклад, виведення на екран інформації зі різними змінними значеннями, що генеруються
або передаються в тіло циклу).
Для перебору масиву даних і виконанні повторюваних дій над ними або відбір певних значень
за умовою (наприклад, пошук чисел, кратних трьом).

2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
for - використовується, як правило, для повторюваних дій з математичним кроком.
while - використовується для виконання дій за умовою. Якщо умова не дійсна -
цикл ініційований не буде.
do/while - використовується для виконання дій за умовою. Якщо умова не дійсна -
цикл буде ініційований і виконає одну ітерацію.

3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
Явне перетворення типів: перетворення, явно передбачене програмою: +number, Boolean(0), toString(1)...
Неявне перетворення: перетворення, неохідне для виконання інструкції, передбаченої програмою,
але не ініційоване явно у коді:
    let str = '1';
    let num = 1;
    console.log(str == num);

*/


/*
Задачі. Перед запуском бажано закоментувати одну із задач.

Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без залишку)
у заданому діапазоні. Завдання має бути виконане на чистому Javascript без використання бібліотек
типу jQuery або React.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа.
  Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
- Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
Необов'язкове завдання підвищеної складності
- Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується,
  повторювати виведення вікна на екран, доки не буде введено ціле число.
*/


console.log('Task 1')

let number = prompt('Enter a number!');

if (isNaN(+number) || number == null || !!(+number - Math.floor(+number))) {
    while (isNaN(+number) || number == null || !!(+number - Math.floor(+number))) {
        number = prompt('Enter a number!');
    }
}

if (+number < 5 && +number > -5) {
    console.log('Sorry, no numbers');
} else if (+number < 0) {
    for (let i = 0; i >= number; i--) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
} else {
    for (let i = 0; i <= number; i++) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
}

/*
Отримати два числа, m і n. Вивести в консоль усі прості числа
(http://uk.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
в діапазоні від m до n (менше із введених чисел буде m, більше буде n). Якщо хоча б одне з чисел
не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати
обидва числа знову.
*/

console.log('Task 2')

/*
Checkup function
*/

function numCheckup(num) {
    if (isNaN(+num) || num <= 0 || num == null || !!(+num - Math.floor(+num))) {
        while (isNaN(+num) || num <= 0 || num == null || !!(+num - Math.floor(+num))) {
            num = prompt('Enter a number 1');
        }
    }

    return(num);
}

/*
Get values
*/

let m = prompt('Enter a number 1');
let startNmb = numCheckup(+m);
let n = prompt('Enter a number 2');
let finishNmb = numCheckup(+n);

/*
Function getSimpleNumbers
*/

function getSimpleNumbers(first, last) {
    while (first !== last) {
        let counter = 0;
        for (let i = 2; i <= first; i++) {
            /*console.log(`startNmb: ${startNmb}, i: ${i} startNmb/i: ${!!(startNmb % i)}`);*/
            if (!(first % i)) {
                counter++;
            }
        }
        if (counter === 1) {
            console.log(first);
        }
        first++;
    }
}

/*
Output
*/

if (startNmb < finishNmb) {
    getSimpleNumbers(startNmb, finishNmb);
} else if (startNmb > finishNmb) {
    getSimpleNumbers(finishNmb, startNmb);
} else {
    console.log('Your numbers is equal!');
}

console.log(`Your first number: ${startNmb}, your second number: ${finishNmb}`);

